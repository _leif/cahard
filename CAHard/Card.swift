//
//  Card.swift
//  CAHard
//
//  Created by Zach Eriksen on 4/9/19.
//  Copyright © 2019 ol. All rights reserved.
//

import UIKit

class Card: UIView {
    // MARK: Lazy
    private lazy var growButton: UIButton = {
        let button = UIButton(frame: buttonFrame)
        button.addTarget(self, action: #selector(grow), for: .touchUpInside)
        button.setTitle("Grow", for: .normal)
        return button
    }()
    private lazy var shrinkButton: UIButton = {
        let button = UIButton(frame: buttonFrame)
        button.addTarget(self, action: #selector(shrink), for: .touchUpInside)
        button.setTitle("Shrink", for: .normal)
        return button
    }()
    // MARK: Calculated
    private var buttonFrame: CGRect {
        return  CGRect(x: bounds.width - 108,
                       y: 8,
                       width: 100,
                       height: 44)
    }
    private var scrollViewYOffset: CGFloat {
        return (superview as? UIScrollView)?.contentOffset.y ?? 0
    }
    // MARK: Private
    private var oldFrame: CGRect!
    private var oldView: UIView!
    private var oldCornerRadius: CGFloat!
    private var isExpanded = false {
        didSet {
            updateButtons()
        }
    }
    // MARK: Super
    override var frame: CGRect {
        didSet {
            updateButtons()
        }
    }
    
    init() {
        super.init(frame: .zero)
        sharedInit()
    }
    
    init(height: CGFloat) {
        super.init(frame: CGRect(origin: .zero,
                                 size: CGSize(width: 0, height: height)))
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func sharedInit() {
        addSubview(growButton)
        addSubview(shrinkButton)
        updateButtons()
    }
    
    public func updateButtons() {
        growButton.frame = buttonFrame
        shrinkButton.frame = buttonFrame
        
        growButton.isHidden = isExpanded
        shrinkButton.isHidden = !isExpanded
        
        bringSubviewToFront(growButton)
        bringSubviewToFront(shrinkButton)
    }
    
    @objc
    private func grow() {
        oldFrame = frame
        oldView = superview
        oldCornerRadius = layer.cornerRadius
        superview?.bringSubviewToFront(self)
        let frame = CGRect(origin: CGPoint(x: oldView.frame.origin.x,
                                           y: oldView.frame.origin.y + scrollViewYOffset),
                           size: oldView.frame.size)
        UIView.animate(withDuration: 0.5) {
            self.frame = frame
            self.layer.cornerRadius = 0
            self.isExpanded = true
        }
    }
    
    @objc
    private func shrink() {
        UIView.animate(withDuration: 0.5) {
            self.frame = self.oldFrame
            self.layer.cornerRadius = self.oldCornerRadius
            self.isExpanded = false
        }
    }
}
