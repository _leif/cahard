//
//  ViewController.swift
//  CAHard
//
//  Created by Zach Eriksen on 4/9/19.
//  Copyright © 2019 ol. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var baseView: UIView!
    let smallFrame = CGRect(x: UIScreen.main.bounds.width / 3,
                            y: UIScreen.main.bounds.height / 4,
                            width: UIScreen.main.bounds.width / 3,
                            height: UIScreen.main.bounds.height / 2)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let one = Card()
        
        one.backgroundColor = .red
        
        let two = Card(height: 600)
        
        two.backgroundColor = .blue
        
        let three = Card()
        
        three.backgroundColor = .green
        
        let cardView = CardControllerView()
        
        cardView.build(withCards: [one, two, three])
        
        baseView.addSubview(cardView)
    }
}
