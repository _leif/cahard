//
//  CardControllerView.swift
//  CAHard
//
//  Created by Zach Eriksen on 4/9/19.
//  Copyright © 2019 ol. All rights reserved.
//

import UIKit

class CardControllerView: UIView {
    private var scrollView: UIScrollView!
    
    var cards: [Card] = []
    
    var rowSpacing: CGFloat = 8
    var colSpacing: CGFloat = 8
    
    var topPadding: CGFloat = 16
    var bottomPadding: CGFloat = 16
    var leadingPadding: CGFloat = 16
    var trailingPadding: CGFloat = 16
    
    var cornerRadius: CGFloat = 8
    
    var defaultHeight: CGFloat = 100
    var defaultWidth: CGFloat {
        return bounds.width - (leadingPadding + trailingPadding)
    }
    
    init() {
        super.init(frame: UIScreen.main.bounds)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        scrollView = UIScrollView(frame: bounds)
        scrollView.backgroundColor = .darkGray
        addSubview(scrollView)
    }
    
    private func updateCards() {
        for card in cards {
            card.layer.cornerRadius = cornerRadius
        }
    }
    
    private func layoutCards() {
        removeAllCards()
        var currentY: CGFloat = topPadding
        for card in cards {
            card.frame.size = CGSize(width: defaultWidth,
                                     height: card.frame.height == 0 ? defaultHeight : card.frame.height)
            card.frame.origin = CGPoint(x: (bounds.width / 2) - (card.bounds.width / 2),
                                        y: currentY)
            scrollView.addSubview(card)
            currentY += card.bounds.height + rowSpacing
        }
    }
    
    private func removeAllCards() {
        cards.forEach { $0.removeFromSuperview() }
    }
    
    private func updateScrollView() {
        guard let last = cards.last else {
            return
        }
        scrollView.contentSize = CGSize(width: 0, height: last.frame.maxY + bottomPadding)
    }
    
    public func build() {
        updateCards()
        layoutCards()
        updateScrollView()
    }
    
    public func build(withCards cards: [Card]) {
        self.cards = cards
        build()
    }

}
